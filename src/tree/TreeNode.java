package tree;

// Definition for binary tree
public class TreeNode {
	public int data;
	public TreeNode left;
	public TreeNode right;

	public TreeNode(int x) {
		data = x;
	}
	
	public String toString() {
		return "Data: " + data;
	}
}
