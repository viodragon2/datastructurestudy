package tree.bst;

import tree.TreeNode;

public class BinarySearchTree {

	public TreeNode Delete(TreeNode root, int data) {
		if (root == null)
			return null;
		else if (root.data < data)
			Delete(root.right, data);
		else if (root.data > data)
			Delete(root.left, data);
		else {
			// Case 1: No child
			if (root.left == null && root.right == null) {
				root = null;
			}
			// Case 2: One child
			else if (root.left == null) { // has right child only
				// TreeNode temp = root;
				root = root.right;
				// temp = null;
			} else if (root.right == null) {
				// TreeNode temp = root;
				root = root.left;
				// temp = null;
			}
			// case 3: Two children
			else {
				TreeNode temp = FindMin(root.right);
				root.data = temp.data;
				root.right = Delete(root.right, temp.data);
			}
		}
		return root;
	}

	/**
	 * Find minimum in BST. Essentially return leftmost TreeNode in BST.
	 * 
	 * @param root
	 * @param data
	 * @return
	 */
	public TreeNode FindMin(TreeNode root) {
		while (root.left != null) {
			root = root.left;
		}
		return root;
	}

	public TreeNode Find(TreeNode root, int data) {
		if (root == null)
			return null;

		if (root.data == data)
			return root;
		else if (root.data < data)
			return Find(root.right, data);

		return Find(root.left, data);
	}

	public TreeNode GetSuccessor(TreeNode root, int data) {
		if (root == null)
			return null;
		// Search node - O(h)
		TreeNode current = Find(root, data);
		if (current == null)
			return null;
		// Case 1: Node has right subtree
		if (current.right != null) {
			// Go deep to the leftmost node in the right subtree OR Find min in
			// right subtree
			return FindMin(current.right); // O(h)
		} 
		// Case 2: Node has no right subtree - O(h)
		else {
			// Go to the nearest ancestor in which given node would be in left
			// subtree
			TreeNode ancestor = root;
			TreeNode successor = null;
			while (ancestor != current) {
				 if (current.data < ancestor.data) {
					 successor = ancestor;
					 ancestor = ancestor.left;
				 }
				 else {
					 ancestor = ancestor.right;
				 }
			}
			return successor;
		}
	}

	public void InorderTraverse(TreeNode root) {
		if (root == null)
			return;

		InorderTraverse(root.left);
		System.out.print(root.data + " ");
		InorderTraverse(root.right);
	}

	public TreeNode Insert(TreeNode root, int data) {
		if (root == null) {
			root = new TreeNode(data);
			// root.data = data;
			root.left = null;
			root.right = null;
			return root;
		} else if (root.data < data)
			root.right = Insert(root.right, data);
		else
			// if (root.data > data)
			root.left = Insert(root.left, data);

		return root;
	}

	public static void main(String[] args) {
		TreeNode root = null;

		//	    	15
		// 	  10 		20
		//   8   12	  17 	25
		// 6	11	 16		   27
		BinarySearchTree bst = new BinarySearchTree();
		root = bst.Insert(root, 15);
		root = bst.Insert(root, 10);
		root = bst.Insert(root, 20);
		root = bst.Insert(root, 8);
		root = bst.Insert(root, 12);
		root = bst.Insert(root, 17);
		root = bst.Insert(root, 25);
		root = bst.Insert(root, 6);
		root = bst.Insert(root, 11);
		root = bst.Insert(root, 16);
		root = bst.Insert(root, 27);
		bst.InorderTraverse(root);

		System.out.println();
		System.out.println("FindMin (right) : " + bst.FindMin(root.right));
		System.out.println("Find(10) : " + bst.Find(root, 10));
		
		System.out.println("GetSuccessor (15) : " + bst.GetSuccessor(root, 15));	// expected: 16
		System.out.println("GetSuccessor (6) : " + bst.GetSuccessor(root, 6));		// expected: 8
		System.out.println("GetSuccessor (12) : " + bst.GetSuccessor(root, 12));	// expected: 15

		bst.Delete(root, 6);
		bst.InorderTraverse(root);
		System.out.println();
		bst.Delete(root, 12);
		bst.InorderTraverse(root);
		System.out.println();
		bst.Delete(root, 20);
		bst.InorderTraverse(root);
	}
}
