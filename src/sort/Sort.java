package sort;

import java.util.Arrays;

import list.LinkedList;
import list.ListNode;

public class Sort {
	/**
	 * This method will sort the integer array using insertion sort algorithm.
	 * The complexity of insertion sorting is O(n) for best case (already sorted
	 * array) and O(n2) for worst case (sorted in reverse order).
	 * 
	 * @param num
	 */
	public static void insertionSort(int[] num) {
		int j; // the number of items sorted so far
		int key; // the item to be inserted
		int i;

		for (j = 1; j < num.length; j++) // Start with 1 (not 0)
		{
			key = num[j];
			for (i = j - 1; (i >= 0) && (num[i] > key); i--) {
				num[i + 1] = num[i];
			}
			num[i + 1] = key; // Put the key in its proper location
		}
	}

	/**
	 * This method will sort the Linked List using insertion sort algorithm.
	 * @param head
	 * @return
	 */
	public static ListNode insertionSortList(ListNode head) {
		// Case 0: list is empty
		// Case 1: list only contains one item
		if (head == null || head.next == null)
			return head;

		// Case 2: list contains two or more items (need comparison)

		// _key_ is the value to be compared to the "sorted list".
		ListNode key = head.next;
		// _head_ is considered to be a "sorted list". Hence next node of head
		// is null at the start.
		head.next = null;
		ListNode itr, h; // itr: copy of current key, h: copy of current head.
							// Both are used to compare and iterate through the
							// list without changing the node.
		while (key != null) {
			// Case 2.1: when value needs to be inserted in front of _head_
			// Test the following with sorted list in descending order (e.g.
			// 5->4->3->2->1 etc.)
			itr = key;
			key = key.next;
			itr.next = null;
			// compare value to the head
			if (head.val > itr.val) {
				// insert in front of head and set itr as head
				itr.next = head;
				head = itr;
				continue;
			}

			// Case 2.2: Insert after head
			// Starting from head, find the slot where _itr_ or the current key
			// needs to be inserted.
			h = head;
			
			// Case 2.2.1: Current value is in between head and the last. Insert before the bigger value.
			while (h.next != null) {
				if (h.next.val > itr.val) {
					itr.next = h.next;
					h.next = itr;
					break;
				}
				h = h.next;
			}
			
			// Case 2.2.2: Current value is the biggest value. Insert at the end of sorted list.
			if (h.next == null) {
				h.next = itr;
			}
		}
		return head;
	}

	public static void printArray(int[] B) {
		System.out.println(Arrays.toString(B));
	}

	public static void populateArray(int[] B) {
		for (int i = 0; i < B.length; i++) {
			B[i] = (int) (Math.random() * 100);
		}
	}

	public static ListNode popuplateList(int length) {
		ListNode head, tmp;
		head = new ListNode((int) (Math.random() * 100));
		tmp = head;
		for (int i = 0; i < length - 1; i++) {
			tmp.next = new ListNode((int) (Math.random() * 100));
			tmp = tmp.next;
		}
		return head;
	}

	public static void main(String[] args) {
		int A[] = new int[10];
		populateArray(A);
		System.out.println("Before Sorting: ");
		printArray(A);
		// sort the array
		insertionSort(A);
		System.out.println("\nAfter Sorting: ");
		printArray(A);

		System.out.println("Before Sorting: ");
		ListNode head = popuplateList(4);
		LinkedList.printList(head);
		System.out.println("\nAfter Sorting: ");
		LinkedList.printList(insertionSortList(head));

		ListNode n1 = new ListNode(2);
		ListNode n2 = new ListNode(3);
		ListNode n3 = new ListNode(4);

		ListNode n4 = new ListNode(3);
		ListNode n5 = new ListNode(4);
		ListNode n6 = new ListNode(5);

		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;

		LinkedList.printList(insertionSortList(n1));
	}
}
